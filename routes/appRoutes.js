var Router= require('koa-router');
var bodyParser = require('koa-body')();

module.exports = function(app){

    var router = new Router();

    //Welcome Routes
    var welcomeCtrl = require('./../controllers/WelcomeCtrl');
    var userCtrl= require('./../controllers/userCtrl');
    var competitionCtrl = require('./../controllers/competitionCtrl');
    var exhibitionCtrl = require('./../controllers/exhibitionCtrl');
    var sendMail = require('./../controllers/sendMail');

    router.get('/exhibition',exhibitionCtrl.showExhibitionPage);
    router.get('/competition',competitionCtrl.showCompetitionPage);
    router.get('/home', welcomeCtrl.showHomePage);
    router.get('/page',welcomeCtrl.showPage);
    router.get('/test',welcomeCtrl.showtest);
    router.post('/addpic',exhibitionCtrl.addPic);
    router.get('/gallery',exhibitionCtrl.showGallery);
    router.post('/signup0',userCtrl.signup0);
    router.post('/login',userCtrl.login);
    router.get('/logout',userCtrl.logout);
    router.post('/competition',competitionCtrl.addCompetition);
    router.get('/artist',userCtrl.showArtistSignupPage);
    router.post('/artistSignup',userCtrl.addArtist);
    router.get('/dashboard',welcomeCtrl.showDashbaord);
    router.get('/forgotpassword',userCtrl.showforgotpasswordpage);
    router.post('/matchOTP',userCtrl.checkOTP);
    router.post('/reset',userCtrl.changePWD);
    router.post('/deleteGallery',exhibitionCtrl.deleteGallery);
    router.post('/addExhibition',exhibitionCtrl.addExhibition);
    router.post('/sendMail',sendMail.sendMail);
   // router.get('/newpass',welcomeCtrl.shownewpass);




    return router.middleware();
}
