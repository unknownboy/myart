var Router= require('koa-router');
var bodyParser = require('koa-body')();

module.exports = function(app){

    var router = new Router();
    var apiCtrl=require('./../controllers/apiCtrl');

    router.get('/getstates',apiCtrl.showstates);
    router.get('/getdistricts',apiCtrl.getdistricts);
    router.post('/getart',apiCtrl.getArt);
    router.post('/getname',apiCtrl.showName);
    router.post('/registerme',apiCtrl.registerMe);
    router.post('/login',apiCtrl.login);
    router.post('/logout',apiCtrl.logout);
    router.post('/getartist',apiCtrl.showArtist);
    router.post('/otp',apiCtrl.sendOtp);
    router.post('/updateValues',apiCtrl.updateValues);
    router.post('/changepwd2',apiCtrl.changePwd);
    router.post('/islogin',apiCtrl.islogin);
    router.post('/updateValues2',apiCtrl.updateValues2);
    router.post('/getplayer',apiCtrl.showPlayer);
    router.post('/updateWinner',apiCtrl.updateWinner);
    router.post('/getWinners',apiCtrl.getWinners);
    return router.middleware();
}
