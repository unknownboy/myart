-- MySQL dump 10.11
--
-- Host: localhost    Database: myart
-- ------------------------------------------------------
-- Server version	5.0.45-community-nt

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `art`
--

DROP TABLE IF EXISTS `art`;
CREATE TABLE `art` (
  `ID` int(10) unsigned NOT NULL auto_increment,
  `TS` timestamp NULL default CURRENT_TIMESTAMP,
  `ACTIVE` tinyint(4) NOT NULL default '1',
  `DEL` tinyint(4) NOT NULL default '0',
  `PIC` varchar(50) collate utf8_bin NOT NULL,
  `USER_ID` int(10) unsigned NOT NULL,
  `AMOUNT` int(10) unsigned NOT NULL,
  `SOLD` tinyint(4) NOT NULL default '0',
  `TAG` varchar(200) collate utf8_bin NOT NULL,
  PRIMARY KEY  (`ID`),
  KEY `USER_ID` (`USER_ID`),
  CONSTRAINT `art_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `art`
--

LOCK TABLES `art` WRITE;
/*!40000 ALTER TABLE `art` DISABLE KEYS */;
/*!40000 ALTER TABLE `art` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `competition`
--

DROP TABLE IF EXISTS `competition`;
CREATE TABLE `competition` (
  `ID` int(10) unsigned NOT NULL auto_increment,
  `TS` timestamp NULL default CURRENT_TIMESTAMP,
  `TITLE` varchar(50) collate utf8_bin NOT NULL,
  `START_TIME` timestamp NOT NULL default '0000-00-00 00:00:00',
  `END_TIME` timestamp NOT NULL default '0000-00-00 00:00:00',
  `PRIZE` varchar(200) collate utf8_bin NOT NULL,
  `WINNER1` int(10) unsigned default NULL,
  `WINNER2` int(10) unsigned default NULL,
  `WINNER3` int(10) unsigned default NULL,
  `PIC` varchar(50) collate utf8_bin NOT NULL,
  `LIMITUSER` int(10) unsigned default NULL,
  `ACTIVE` tinyint(4) NOT NULL default '1',
  `DEL` tinyint(4) NOT NULL default '0',
  PRIMARY KEY  (`ID`),
  UNIQUE KEY `TITLE` (`TITLE`),
  KEY `WINNER1` (`WINNER1`),
  KEY `WINNER2` (`WINNER2`),
  KEY `WINNER3` (`WINNER3`),
  CONSTRAINT `competition_ibfk_1` FOREIGN KEY (`WINNER1`) REFERENCES `user` (`id`),
  CONSTRAINT `competition_ibfk_2` FOREIGN KEY (`WINNER2`) REFERENCES `user` (`id`),
  CONSTRAINT `competition_ibfk_3` FOREIGN KEY (`WINNER3`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `competition`
--

LOCK TABLES `competition` WRITE;
/*!40000 ALTER TABLE `competition` DISABLE KEYS */;
/*!40000 ALTER TABLE `competition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feedback`
--

DROP TABLE IF EXISTS `feedback`;
CREATE TABLE `feedback` (
  `ID` int(10) unsigned NOT NULL auto_increment,
  `TS` timestamp NULL default CURRENT_TIMESTAMP,
  `ACTIVE` tinyint(4) NOT NULL default '1',
  `DEL` tinyint(4) NOT NULL default '0',
  `ART_ID` int(10) unsigned NOT NULL,
  `USER_ID` int(10) unsigned NOT NULL,
  `REACT` tinyint(4) NOT NULL,
  `REVIEW` varchar(200) collate utf8_bin default NULL,
  PRIMARY KEY  (`ID`),
  KEY `USER_ID` (`USER_ID`),
  KEY `ART_ID` (`ART_ID`),
  CONSTRAINT `feedback_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`id`),
  CONSTRAINT `feedback_ibfk_2` FOREIGN KEY (`ART_ID`) REFERENCES `art` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `feedback`
--

LOCK TABLES `feedback` WRITE;
/*!40000 ALTER TABLE `feedback` DISABLE KEYS */;
/*!40000 ALTER TABLE `feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gallery`
--

DROP TABLE IF EXISTS `gallery`;
CREATE TABLE `gallery` (
  `ID` int(10) unsigned NOT NULL auto_increment,
  `TS` timestamp NULL default CURRENT_TIMESTAMP,
  `ACTIVE` tinyint(4) NOT NULL default '1',
  `DEL` tinyint(4) NOT NULL default '0',
  `PIC` varchar(50) collate utf8_bin NOT NULL,
  `DES` varchar(256) collate utf8_bin default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `gallery`
--

LOCK TABLES `gallery` WRITE;
/*!40000 ALTER TABLE `gallery` DISABLE KEYS */;
INSERT INTO `gallery` VALUES (24,'2018-08-06 19:47:13',1,0,'upload_f5a799aa8a143d4ed488d0a468f8516e','Musium visit'),(25,'2018-08-06 19:47:43',1,0,'upload_c9a84cfd067484a84db00a4528e6c53d','neeraj sir'),(26,'2018-08-06 19:48:35',1,0,'upload_c34e80f033da10029ae703824551a3a5','Art Gallery View'),(27,'2018-08-06 19:49:09',1,0,'upload_3de255d781df9e0a095c7a176be76b0f',''),(28,'2018-08-06 19:49:20',1,0,'upload_030f617fe3a774516a756096748769a6',''),(29,'2018-08-06 19:49:31',1,0,'upload_df00c0d80fb2742c8f42442b264da834',''),(30,'2018-08-06 20:02:00',1,0,'upload_4909b9c1ae8cb73e71f4397c5d6a1fc0',''),(31,'2018-08-06 20:02:39',1,0,'upload_6543cc8104813ca799fa90ac3a09c136',''),(32,'2018-08-06 20:02:50',1,0,'upload_624ef3e720b25d65209843d995fe1108',''),(33,'2018-08-06 20:03:02',1,0,'upload_de7d92e59636a21a596a51d818480853',''),(34,'2018-08-06 20:03:19',1,0,'upload_b4a1b8cfbac9a481dbaabac5fc3a47d2',''),(35,'2018-08-06 20:04:41',1,0,'upload_53336b4a5e23ed220543714b88e2fe2f',''),(36,'2018-08-06 20:05:00',1,0,'upload_a408dfeae292c4b1cff5a1b7868a73a0',''),(37,'2018-08-06 20:05:09',1,0,'upload_c88f0c55f52cf57f30015e16215f92b4',''),(38,'2018-08-06 20:05:19',1,0,'upload_8078a46bdb4a7b10c25620ee31aa900b',''),(39,'2018-08-06 20:06:30',1,0,'upload_6802eac83e99fe81e384fa2072b56468','');
/*!40000 ALTER TABLE `gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registration`
--

DROP TABLE IF EXISTS `registration`;
CREATE TABLE `registration` (
  `ID` int(10) unsigned NOT NULL auto_increment,
  `TS` timestamp NULL default CURRENT_TIMESTAMP,
  `ACTIVE` tinyint(4) NOT NULL default '1',
  `DEL` tinyint(4) NOT NULL default '0',
  `COMPETITION_ID` int(10) unsigned NOT NULL,
  `USER_ID` int(10) unsigned NOT NULL,
  `ROLE_ID` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`ID`),
  KEY `USER_ID` (`USER_ID`),
  KEY `ROLE_ID` (`ROLE_ID`),
  KEY `COMPETITION_ID` (`COMPETITION_ID`),
  CONSTRAINT `registration_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`id`),
  CONSTRAINT `registration_ibfk_2` FOREIGN KEY (`ROLE_ID`) REFERENCES `role` (`id`),
  CONSTRAINT `registration_ibfk_3` FOREIGN KEY (`COMPETITION_ID`) REFERENCES `competition` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `registration`
--

LOCK TABLES `registration` WRITE;
/*!40000 ALTER TABLE `registration` DISABLE KEYS */;
/*!40000 ALTER TABLE `registration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `ID` int(10) unsigned NOT NULL auto_increment,
  `TS` timestamp NULL default CURRENT_TIMESTAMP,
  `NAME` varchar(50) collate utf8_bin NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'2018-07-23 17:57:28','ADMIN'),(2,'2018-07-23 17:57:28','COORDINATOR'),(3,'2018-07-23 17:57:28','VENDOR'),(4,'2018-07-23 17:57:28','CUSTOMER');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `ID` int(10) unsigned NOT NULL auto_increment,
  `TS` timestamp NULL default CURRENT_TIMESTAMP,
  `NAME` varchar(50) collate utf8_bin NOT NULL,
  `EMAIL` varchar(50) collate utf8_bin NOT NULL,
  `MOBILE` decimal(10,0) NOT NULL,
  `ADDRESS` varchar(150) collate utf8_bin default NULL,
  `STATE` varchar(70) collate utf8_bin default NULL,
  `CITY` varchar(50) collate utf8_bin default NULL,
  `ACTIVE` tinyint(4) NOT NULL default '1',
  `DEL` tinyint(4) NOT NULL default '0',
  `PIC` varchar(100) collate utf8_bin default NULL,
  `PASSWORD` varchar(50) collate utf8_bin NOT NULL,
  `MYART` int(10) unsigned default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'2018-08-06 18:49:45','Gaurang Sharma','gaurang.sharma_cs16@gla.ac.in','9456437065',NULL,NULL,NULL,1,0,'upload_62896400858cd075110aa9649de267c1','gauri@123',NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `ID` int(10) unsigned NOT NULL auto_increment,
  `TS` timestamp NULL default CURRENT_TIMESTAMP,
  `USER_ID` int(10) unsigned NOT NULL,
  `ROLE_ID` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`ID`),
  KEY `USER_ID` (`USER_ID`),
  KEY `ROLE_ID` (`ROLE_ID`),
  CONSTRAINT `user_role_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `user` (`ID`),
  CONSTRAINT `user_role_ibfk_2` FOREIGN KEY (`ROLE_ID`) REFERENCES `role` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` VALUES (1,'2018-08-06 18:49:45',1,1);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-07  9:28:59
