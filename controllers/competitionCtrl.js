var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils = require('../utils/databaseUtils');
module.exports = {

    showCompetitionPage: function*(next){
        var competition=yield databaseUtils.executeQuery(util.format('select * from competition'));

        var pastcompetition=yield databaseUtils.executeQuery(util.format('select * from competition where (end_time)<now()'));

        var presentcompetition=yield databaseUtils.executeQuery(util.format('select * from competition where (start_time)<=now() and  (end_time)>=now()'));

        var futurecompetition=yield databaseUtils.executeQuery(util.format('select * from competition where (start_time)>now()'));

        var coordinators=yield databaseUtils.executeQuery(util.format('select * from user u,user_role ur where u.id=ur.user_id and ur.role_id in (1,2,3)'));
        var regist;
        if(this.currentUser)
        regist=yield databaseUtils.executeQuery(util.format('select * from registration where user_id=%s',this.currentUser.ID));

        console.log('Lengths are: ',pastcompetition.length,presentcompetition.length,futurecompetition.length);


        yield this.render('contest',{
            competition:competition,
            pastcompetition:pastcompetition,
            presentcompetition:presentcompetition,
            futurecompetition:futurecompetition,
            regist:regist,
            coordinators:coordinators,

        });
    },
    addCompetition:function*(next){
        var title=this.request.body.fields.title;
        var startdate=this.request.body.fields.startdate;
        var starttime=this.request.body.fields.starttime;
        var enddate=this.request.body.fields.enddate;
        var endtime=this.request.body.fields.endtime;
        var prize=this.request.body.fields.prize;
        var coor=this.request.body.fields.coor;
        var userlimit=this.request.body.fields.participants;
        var pic=this.request.body.files.pic;
        pic=pic.path.split('\\')[3];
        console.log(title,startdate+' '+starttime,enddate+' '+endtime,prize,pic,userlimit,coor);
        try{
        var res=yield databaseUtils.executeQuery(util.format('insert into competition (title,start_time,end_time,prize,pic,limituser,coordinator_id) values("%s","%s","%s","%s","%s",%s,%s)',title,startdate+' '+starttime,enddate+' '+endtime,prize,pic,userlimit,coor));
        }
        catch(e){
            console.log(e);
        }
        this.redirect('/app/competition');
    
    }
    
}
