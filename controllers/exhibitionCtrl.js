var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils = require('../utils/databaseUtils');
module.exports = {
showExhibitionPage:function*(next){
    var arts=yield databaseUtils.executeQuery(util.format('select * from art'));
   // var gallery=yield databaseUtils.executeQuery(util.format('SELECT * FROM GALLERY'));
   
    yield this.render('exhibition',{
        gallery:arts,
    });
},
showGallery:function*(next){


    var gallery=yield databaseUtils.executeQuery(util.format('select * from gallery where del=0'));
    yield this.render('gallery',{
        gallery:gallery,
    });
},
addPic:function*(next){
    var pic=this.request.body.files.pic;
    var description=this.request.body.fields.description;
    try{
        var filename=pic.path.split('\\')[3];
        
        var res=yield databaseUtils.executeQuery(util.format('insert into gallery (des,pic) values("%s","%s")',description,filename));
    }
    catch(e){
        console.log(e);
        this.redirect('/app/gallery');
    }
    this.redirect('/app/gallery');
},
deleteGallery:function*(next){
    var delId=this.request.body.myId;
    console.log('gallery ki id',delId);
    var res=yield databaseUtils.executeQuery(util.format('update gallery set del=1 where id=%s',delId));
    this.redirect('/app/gallery');
},
addExhibition:function*(next){
    try{
        var pic=this.request.body.files.pic.path.split('\\')[3];
    var title=this.request.body.fields.title;
    var amount=this.request.body.fields.amount;
    var size=this.request.body.fields.size;
    console.log(pic,this.currentUser.ID,amount,0,title,size,1,0);
    var res=yield databaseUtils.executeQuery(util.format('insert into art (pic,user_id,amount,sold,tag,size,active,del) values("%s",%s,%s,%s,"%s","%s",%s,%s)',pic,this.currentUser.ID,amount,0,title,size,1,0));
 
}catch(e){
console.log(e);
}

this.redirect('/app/exhibition');
}
}
