var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils = require('../utils/databaseUtils');
var redisUtils = require('../utils/redisUtils');

// added
'use strict';
const nodemailer = require('nodemailer');
var rn = require('random-number');
module.exports = {

    login: function*(next){

        var email=this.request.body.email;
        var pwd=this.request.body.pwd;
        var user=yield databaseUtils.executeQuery(util.format('select u.*,r.name as role,r.id as role_id from user u,role r,user_role ur where u.id=ur.user_id and r.id=ur.role_id and u.email="%s" and u.password="%s"',email,pwd));
        if(user.length!=0){
            sessionUtils.saveUserInSession(user[0],this.cookies);
            this.redirect('/app/home');
        }
        else{
            var msg='Wrong Email or Password';
            yield this.render('page',{
                msg:msg,
            });
        }
    },
    signup0: function*(next){
        var iid=0;
        var errormsg;
            var name=this.request.body.name;
            var email=this.request.body.email;
            var phone=this.request.body.phone;
            var pwd=this.request.body.pwd;
            var pwd2=this.request.body.pwd2;
            if(pwd==pwd2){

            
            try{
            var res=yield databaseUtils.executeQuery(util.format('insert into user (name,email,mobile,password) values("%s","%s","%s","%s")',name,email,phone,pwd));
            iid=res.insertId;
            var res=yield databaseUtils.executeQuery(util.format('insert into user_role (user_id,role_id) values(%s,%s)',iid,4));

            var user=yield databaseUtils.executeQuery(util.format('select u.*,r.name as role,r.id as role_id from user u,role r,user_role ur where u.id=ur.user_id and r.id=ur.role_id and u.email="%s" and u.password="%s"',email,pwd));

            sessionUtils.saveUserInSession(user[0],this.cookies);
        

            }catch(e){
                
                console.log('error hai ye',e);
                var res=yield databaseUtils.executeQuery(util.format('delete from user where id=%s',iid));
                var res=yield databaseUtils.executeQuery(util.format('delete from user_role where user_id=%s',iid));
                errormsg='Error in Signup...!!!';
            }
        }
        else{
            errormsg='Password Not Matched..!!!';
        }
        console.log('error msg ki value',errormsg);
            // var fname=this.request.body.fields.fname;
            // var lname=this.request.body.fields.lname;
            // var email=this.request.body.fields.email;
            // var mobile=this.request.body.fields.mobile;
            // var pwd=this.request.body.fields.pwd;
            // var address=this.request.body.fields.address;
            // var state=this.request.body.fields.state;
            // var city=this.request.body.fields.city;
            // var pic=this.request.body.files.pic;
            // pic=pic.split('\\')[2];
            // try{
            //     var res=yield databaseUtils.executeQuery(util.format('INSERT INTO USER (FNAME,LNAME,EMAIL,MOBILE,PASSWORD,ADDRESS,STATE,CITY,PIC) VALUES("%s","%s","%s","%s","%s")',fname,lname,email,mobile,pwd,address,state,city,pic));
            //     iid=res.insertId;
            //     var res=yield databaseUtils.executeQuery(util.format('INSERT INTO USER_ROLE (USER_ID,ROLE_ID) VALUES(%s,%s)',iid,role));
            //     }catch(e){
            //         var res=yield databaseUtils.executeQuery(util.format('DELETE FROM USER WHERE ID=%s',iid));
            //         var res=yield databaseUtils.executeQuery(util.format('DELETE FROM USER_ROLE WHERE USER_ID=%s',iid));
            //         errormsg='Error in Signup...!!!';
            //     }

            var gallery=yield databaseUtils.executeQuery(util.format('select * from gallery'));
            this.redirect('/app/home');
        // if(errormsg){
        //     yield this.render('home',{
        //         errormsg:errormsg,
        //         gallery:gallery,
        //     });
        // }
        // else{
        //     yield this.render('home',{
        //         msg:'You have successfully Signed Up...!!!',
        //         gallery:gallery,
        //     });
        // }
    },

    logout: function* (next) {
        var sessionId = this.cookies.get("SESSION_ID");
        if(sessionId) {
            sessionUtils.deleteSession(sessionId);
        }
        this.cookies.set("SESSION_ID", '', {expires: new Date(1), path: '/'});

        this.redirect('/app/home');
    },
    showArtistSignupPage:function*(next){
        var user;
       
        try{  if(this.currentUser){
            user=yield databaseUtils.executeQuery(util.format('select * from user where id=%s',this.currentUser.ID));
            user=user[0]; }
        }
        catch(e){
            console.log('error ye hai',e);
        }
        yield this.render('artist',{
                user:user,
        }); 
    },
    addArtist:function*(next){

        var name=this.request.body.fields.name;
        var phone=this.request.body.fields.phone;
        var city=this.request.body.fields.city;
        var price=this.request.body.fields.price;
      //  var desc=this.request.body.fields.desc;
        var email=this.request.body.fields.email;
        var address=this.request.body.fields.address;
        var pwd=this.request.body.fields.pwd;
        var pwd2=this.request.body.fields.pwd2;
        var country=this.request.body.fields.country;
        var pic=this.request.body.files.pic;
        var state=this.request.body.fields.state;
        pic=pic.path.split('\\')[3];
        if(pwd==pwd2){
            try{
                var res=yield databaseUtils.executeQuery(util.format('insert into user (name,email,mobile,address,city,pic,password,country,state) values("%s","%s","%s","%s","%s","%s","%s","%s","%s")',name,email,phone,address,city,pic,pwd,country,state));
                var re=yield databaseUtils.executeQuery(util.format('insert into user_role (user_id,role_id) values(%s,%s)',res.insertId,3));
                var user=yield databaseUtils.executeQuery(util.format('select u.*,r.name as role,r.id as role_id from user u,role r,user_role ur where u.id=ur.user_id and r.id=ur.role_id and u.email="%s" and u.password="%s"',email,pwd));
            sessionUtils.saveUserInSession(user[0],this.cookies);
        
            }
            catch(e){
                console.log('nayi waali error',e);

                try{
var res=yield databaseUtils.executeQuery(util.format('update user set address="%s",city="%s",country="%s",pic="%s" where id=%s',address,city,country,pic,this.currentUser.ID));
var res=yield databaseUtils.executeQuery(util.format('update user_role set role_id=3 where user_id=%s',this.currentUser.ID));
                }
                catch(e){
                    console.log('ye waali error',e);
                }
            }
        }

        this.redirect('/app/home');

    },
    recover:function*(next){

        nodemailer.createTestAccount((err, account) => {
            // create reusable transporter object using the default SMTP transport
            let transporter = nodemailer.createTransport({
                host: 'smtp.gmail.com',
                port: 587,
                secure: false, // true for 465, false for other ports
                auth: {
                    user: 'glagalleryofmodernart@gmail.com', // generated ethereal user
                    pass: 'aggkjaggkj' // generated ethereal password
                }
            });
            
            var gen = rn.generator({
              min:  100000
            , max:  999999
            , integer: true
            });
            // setup email data with unicode symbols
            let mailOptions = {
                from: 'glagalleryofmodernart@gmail.com', // sender address
                to: 'testmail1615000@gmail.com', // list of receivers
                subject: 'Password Recovery Mail', // Subject line
                text: 'Your Code For Recovery is '+gen(), // plain text body
                html: '' // html body
            };
        
            // send mail with defined transport object
            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    return console.log(error);
                }
                console.log('Message sent: %s', info.messageId);
                // Preview only available when sending through an Ethereal account
                console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
        
                // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
                // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
            });
        });


    },
    showforgotpasswordpage:function*(next){
        yield this.render('forgotpassword',{

        });
    },
    checkOTP:function*(next){
        var email=this.request.body.email;
        var otp=this.request.body.otp;
        var page='error';
        var msg;
        var res=yield databaseUtils.executeQuery(util.format('select otp,email from fp where email="%s" and otp="%s" and  ts-now()<=120',email,otp));
        if(res.length==0){
            msg='Cannot change password';
            yield this.render('error',{
                email:email,
                msg:msg,
            });
        }
        else{
            
            yield this.render('newpass',{
                email:email,
                msg:msg,
            });
        }



        
    },
    changePWD:function*(next){
        var msg;
        var email=this.request.body.email;
        var pwd=this.request.body.pwd;
        try{
            var res=yield databaseUtils.executeQuery(util.format('update user set password="%s" where email="%s"',pwd,email));
            this.redirect('/app/home');
        }
        catch(e){
            console.log('password change nahi hua',e);
            msg='Cannot change Password';
            
        }
        yield this.render('error',{
            msg:msg,
        })
    }

}
