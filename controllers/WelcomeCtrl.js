var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils = require('../utils/databaseUtils');
module.exports = {
    showHomePage: function* (next) {

        // var landing=yield databaseUtils.executeQuery(util.format('SELECT * FROM GALLERY where tag=\'landing\''));
        // var contest=yield databaseUtils.executeQuery(util.format('SELECT * FROM competition'));
         var gallery=yield databaseUtils.executeQuery(util.format('select * from gallery where del=0'));

         var presentcompetition=yield databaseUtils.executeQuery(util.format('select * from competition where (start_time)<=now() and  (end_time)>=now()'));

         var futurecompetition=yield databaseUtils.executeQuery(util.format('select * from competition where (start_time)>now()'));

         var errormsg; var msg;
       // console.log(gallery);
        yield this.render('home',{
            // landing:landing,
            // contest:contest,
             gallery:gallery,
             errormsg:errormsg,
             msg:msg,
             presentcompetition:presentcompetition,
             futurecompetition:futurecompetition,
             

        });
    },
    showPage:function*(next){
        yield this.render('page',{

        });
    },

    showtest :function*(next){
        yield this.render('test',{

        });
    },


    showDashbaord:function*(next){

        var user;
        var sold;
        var pastactivity;
        var mypaintings;
        try{
        user=yield databaseUtils.executeQuery(util.format('select u.*,r.name as role,r.id as role_id from user u,role r,user_role ur where u.id=ur.user_id and r.id=ur.role_id and u.id=%s',this.currentUser.ID));
         sold=yield databaseUtils.executeQuery(util.format('select count(*) as c from art where user_id=%s and sold=1',this.currentUser.ID));
         pastactivity=yield databaseUtils.executeQuery(util.format('select distinct(competition_id),c.* from registration r right join competition c on r.competition_id=c.id where r.user_id=%s',this.currentUser.ID));
            mypaintings=yield databaseUtils.executeQuery(util.format('select * from art where user_id=%s',this.currentUser.ID));
            user=user[0];
            sold=sold[0];
        }
        catch(e){

            console.log(e);
        }

        // console.log(user[0]);



        yield this.render('dashboard',{
            user:user,
            sold:sold,
            pastactivity:pastactivity,
            mypaintings:mypaintings,
        });
    },
    shownewpass:function*(next){
        yield this.render('newpass',{
            email:'abc',
        });
    }

}
