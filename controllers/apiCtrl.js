var sessionUtils = require('../utils/sessionUtils');
var util=require('util');
var databaseUtils = require('../utils/databaseUtils');
var state= require('../controllers/state');
// added
'use strict';
const nodemailer = require('nodemailer');
var rn = require('random-number');

module.exports = {
    showstates: function*(next){
        var s=state.state;
        var arr=[];
        for(i in s) arr.push(i);
        arr.sort();
        this.body=arr;
    },
    getdistricts: function*(next){
        var s=state.state;
        var ss=this.request.query.state;
        this.body=s[ss];
    },
    getArt:function*(next){
        var aid=this.request.body.aid;
        if(aid){
            var res=yield databaseUtils.executeQuery(util.format('SELECT * FROM ART WHERE ID=%s',aid));
            this.body={data:res[0]}
           // this.body={data:1}
        }
        else{
            this.body={data:0}
        }
    },
    showName:function*(next){
        try{
        var name=yield databaseUtils.executeQuery(util.format('select name from user where id=%s',this.currentUser.ID));
        if(name.length!=0)
        this.body={name:name[0].NAME}
        }
        catch(e){

        }
    },
    registerMe:function*(next){
        try{
        var pid=this.currentUser.ID;
        var id=this.request.body.id;
        var rid=this.currentUser.ROLE_ID;
        var res=yield databaseUtils.executeQuery(util.format('insert into registration (competition_id,user_id,role_id) values(%s,%s,%s)',id,pid,rid));
        this.body={flag:1}
        }
        catch(e){
            this.body={flag:0}
        }
    },
    login: function*(next){

        var email=this.request.body.email;
        var pwd=this.request.body.pwd;
        console.log(email,pwd);
        var user=yield databaseUtils.executeQuery(util.format('select u.*,r.name as role,r.id as role_id from user u,role r,user_role ur where u.id=ur.user_id and r.id=ur.role_id and u.email="%s" and u.password="%s"',email,pwd));
        if(user.length!=0){
            sessionUtils.saveUserInSession(user[0],this.cookies);
            this.body={name:user[0].NAME,flag:1}
            console.log('andar bhi aaya');
        }
        else{
            this.body={flag:0}
        }
        console.log('api chali');
    },
    logout: function* (next) {
        var sessionId = this.cookies.get("SESSION_ID");
        if(sessionId) {
            sessionUtils.deleteSession(sessionId);
        }
        this.cookies.set("SESSION_ID", '', {expires: new Date(1), path: '/'});
        
    },
    showArtist:function*(next){
        var id=this.request.body.id;
        var res=yield databaseUtils.executeQuery(util.format('select u.* from user u,art a where a.user_id=u.id and a.id=%s',id));
        this.body={data:res[0]}
    },
    sendOtp:function*(next){

        try{
        var email=this.request.body.email;
            var otp;

            var gen = rn.generator({
                min:  100000
              , max:  999999
              , integer: true
              });
              otp=gen();
              var res=yield databaseUtils.executeQuery(util.format('insert into fp (email,otp) values("%s","%s")',email,otp));
        nodemailer.createTestAccount((err, account) => {
            // create reusable transporter object using the default SMTP transport
            let transporter = nodemailer.createTransport({
                host: 'smtp.gmail.com',
                port: 587,
                secure: false, // true for 465, false for other ports
                auth: {
                    user: 'glagalleryofmodernart@gmail.com', // generated ethereal user
                    pass: 'aggkjaggkj' // generated ethereal password
                }
            });
            
            
            // setup email data with unicode symbols
            let mailOptions = {
                from: 'glagalleryofmodernart@gmail.com', // sender address
                to: email, // list of receivers
                subject: 'Password Recovery Mail', // Subject line
                text: 'Your Code For Recovery is '+otp, // plain text body
                html: '' // html body
            };
            console.log(email,otp);
            // redisUtils.setItemWithExpiry(email,  JSON.stringify(otp),120);
            
            // send mail with defined transport object
            transporter.sendMail(mailOptions, (error, info) => {
                
                if (error) {
                    return console.log(error);
                }
                console.log('Message sent: %s', info.messageId);
                // Preview only available when sending through an Ethereal account
                console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
        
                // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
                // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
            });
        });


      // redisUtils.setItemWithExpiry( email,120,otp);

        this.body={flag:1}
    }
    catch(e){
        console.log(e);
        this.body={flag:0}
    }
    },
    updateValues:function*(next){
        var id=this.request.body.fields.id;

        var pic=this.request.body.files.pic;
        // console.log(pic);
        var fd=this.request.body.fields.fd;
        try{
            var res=yield databaseUtils.executeQuery(util.format('update user set pic="%s" where id=%s',pic.path.split('\\')[3],this.currentUser.ID));
            var user=yield databaseUtils.executeQuery(util.format('select u.*,r.name as role,r.id as role_id from user u,role r,user_role ur where u.id=ur.user_id and r.id=ur.role_id and u.id=%s',this.currentUser.ID));
            sessionUtils.updateUserInSession(user[0],this.cookies);
            this.body={flag:1}
        }
        catch(e){
            console.log(e);
            this.body={flag:0}
        }
    },
    updateValues2:function*(next){
        var id=this.request.body.id;
        var name=this.request.body.name;
        var phone=this.request.body.phone;
        var email=this.request.body.email;
        try{
            var res=yield databaseUtils.executeQuery(util.format('update user set name="%s",mobile="%s",email="%s" where id=%s',name,phone,email,this.currentUser.ID));
            var user=yield databaseUtils.executeQuery(util.format('select u.*,r.name as role,r.id as role_id from user u,role r,user_role ur where u.id=ur.user_id and r.id=ur.role_id and u.id=%s',this.currentUser.ID));
            sessionUtils.updateUserInSession(user[0],this.cookies);
            this.body={flag:1,user:user}
        }
        catch(e){
            console.log('error aayi',e);
            this.body={flag:0}
        }

    },
    changePwd:function*(next){
        var id=this.request.body.id;
        var pwd1=this.request.body.pwd1;
        var pwd2=this.request.body.pwd2;
        var pwd3=this.request.body.pwd3;
        if(pwd2==pwd3){
            try{
                var res=yield databaseUtils.executeQuery(util.format('update user set password="%s" where password="%s" and id=%s',pwd2,pwd1,id));
                if(res.length!=0) this.body={flag:1}
                else this.body={flag:0}
            }
            catch(e){
                this.body={flag:0}
            }
        }
        else{
            this.body={flag:0}
        }
    },
    islogin:function*(next){
        if(this.currentUser){
            this.body={flag:1,pic:this.currentUser.PIC,name:this.currentUser.NAME}
        }
        else{
            this.body={flag:0}
        }
    },

    showPlayer:function*(next){

        var conId=this.request.body.contestId;
        var res=yield databaseUtils.executeQuery(util.format('select u.name,u.id from user u,competition c where c.id=%s',conId));
        this.body=res;
    },
    updateWinner:function*(next){
        var win1=this.request.body.winner1;
        var win2=this.request.body.winner2;
        var win3=this.request.body.winner3;
        var conId=this.request.body.contestId;
        var res=yield databaseUtils.executeQuery(util.format('update competition set winner1=%s,winner2=%s,winner3=%s where id=%s',win1,win2,win3,conId));
        this.body={flag:1}
    },
    getWinners:function*(next){
        var contestId=this.request.body.contestId;
        var res=yield databaseUtils.executeQuery(util.format('select * from competition where id=%s',contestId));
        this.body=res;
    }

}
