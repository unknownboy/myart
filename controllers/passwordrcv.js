'use strict';
const nodemailer = require('nodemailer');
var rn = require('random-number');

// Generate test SMTP service account from ethereal.email
// Only needed if you don't have a real mail account for testing
nodemailer.createTestAccount((err, account) => {
    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
            user: 'glagalleryofmodernart@gmail.com', // generated ethereal user
            pass: 'aggkjaggkj' // generated ethereal password
        }
    });
    
    var gen = rn.generator({
      min:  100000
    , max:  999999
    , integer: true
    });
    // setup email data with unicode symbols
    let mailOptions = {
        from: 'glagalleryofmodernart@gmail.com', // sender address
        to: 'testmail1615000@gmail.com', // list of receivers
        subject: 'Password Recovery Mail', // Subject line
        text: 'Your Code For Recovery is '+gen(), // plain text body
        html: '' // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message sent: %s', info.messageId);
        // Preview only available when sending through an Ethereal account
        console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

        // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
        // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
    });
});